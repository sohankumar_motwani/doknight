<?php
require_once('../../includes/database_master.inc.php');
include '../../includes/error_master.inc.php';

$database_master = new DatabaseMaster();

$id = $_GET['id'];


if(empty($_GET['id'])) 
{
echo "Park!";
}
else
{
    $qryUser = "SELECT * FROM tblusers WHERE ID = '$id'";
    $rsltUser = $database_master->querySelect($qryUser);
    
    echo $rsltUser[0]['FirstName'];

}

$qryRoles = "SELECT ID, Type FROM tblroles";
$rsltRoles = $database_master->querySelect($qryRoles);
$qryLocations = "SELECT ID, Location FROM tblLocations";
$rsltLocations = $database_master->querySelect($qryLocations);
?>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Create an Account | THC</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" type="text/css" media="screen" href="../../assets/styles/main.css" />
    <link rel="stylesheet" type="text/css" media="screen" href="../../assets/styles/bootstrap.min.css" />
<script src="../../assets/scripts/main.js"></script>
</head>
<body>
    <h1>Create an Account</h1>
<ul><li><?php echo $message; ?></li></ul>
<form action="process/create-submit.php" method="post">
<label>First Name <span>*</span>: </label>
<input type="text" name="firstName" required="required" id="firstName" maxlength="50" placeholder="John"/>
<br/>
<label>Last Name <span>*</span>: </label>
<input type="text" name="lastName" required="required" id="lastName" maxlength="50" placeholder="Doe"/>
<br/>
<label>E-mail <span>*</span>: </label>
<input type="email" name="email" required="required" id="email" maxlength="50" placeholder="someone@example.com"/>
<br/>
<label>Role <span>*</span>: </label>
<select required="required" name="role">
<option value="">Select...</option>
<?php
if(count($rsltRoles) && is_array($rsltRoles))
{
foreach($rsltRoles as $key => $result)
{
echo "<option value='" . $result['ID'] . "'>" . $result['Type'] . "</option>";
}
}
?>
</select>
<br/>
<label>Location <span> *</span>: </label>
<select required="required" name="location">
<option value="">Select...</option>
<?php
if(count($rsltLocations) && is_array($rsltLocations))
{
foreach($rsltLocations as $key => $result)
{
echo "<option value='" . $result['ID'] . "'>" . $result['Location'] . "</option>";
}
}
?>
</select>
<br/>
<input type="submit" value="Submit" />
</form>
</body>
</html>