<?php
require_once('includes/page_master.inc.php');

$page_master = new PageMaster();

$email = $_POST['email'];
$password = $_POST['password'];

if(empty($email) || empty($password))
{
$page_master->redirectUser("../../../index.php?error=incomplete");
}
else
{
    require_once('includes/database_master.inc.php');
require_once('includes/session_master.inc.php');

$database_master = new DatabaseMaster();
$session_master = new SessionMaster();

$email = $database_master->escapeString($email);
$password = $database_master->escapeString($password);

$qryCheckUserType = "SELECT u.UserID, u.UserTypeID, t.Type FROM users as u INNER JOIN usertypes as t on u.UserTypeID = t.UserTypeID WHERE u.Email = '$email' AND u.Password = SHA2('$password', 224)";
$rsltUserType = $database_master->querySelect($qryCheckUserType);

if(is_array($rsltUserType) && count($rsltUserType))
{
$session_master->logUserIn($rsltUserType[0]["UserID"]);
if($rsltUserType[0]['Type'] == "Administrator")
{
$page_master->redirectUser("modules/dashboard/index.php");
}
else if($rsltUserType[0]['Type'] == "Donor")
{
    $page_master->redirectUser("modules/donors/index.php");
}
/*
else if($rsltUserType['Type'] == "Staff")
{
    $page_master->redirectUser("modules/sales/index.php");
} //closing redirection based on user type.
*/
}
else
{
$page_master->redirectUser("../../../index.php?error=incorrect");
}
} //post empty check.
?>