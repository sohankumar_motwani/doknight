<?php
	session_start();
	class SessionMaster{
		public function isLoggedIn(){
			if(isset($_SESSION['logged'])){
				if($_SESSION['logged'] == true)
					return true;
				else return false;
			}
			else return false;
		}
		public function logUserIn($username){
			$_SESSION['username']= $username;
			$_SESSION['logged']= true;
		}

		public function logUserOut(){
			unset($_SESSION['username']);
			unset($_SESSION['logged']);
			$_SESSION = array();
		}
	}
?>