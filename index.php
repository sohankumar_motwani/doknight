<?php include './includes/error_master.inc.php'; ?>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Login</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" type="text/css" media="screen" href="assets/styles/main.css" />
    <link rel="stylesheet" type="text/css" media="screen" href="assets/styles/bootstrap.min.css" />
    <script src="assets/scripts/main.js"></script>
</head>
<body>
    <h1>Login</h1>
    <ul><li><?php echo $message; ?></li></ul>
<form action="modules/accounts/process/login-user.php" method="post">
<label>E-mail <span>*</span>: </label>
<input type="text" name="email" required="required" id="email" maxlength="100" placeholder="someone@example.com"/>
<br/>
<label>Password <span>*</span>: </label>
<input type="password" name="password" required="required" id="email" maxlength="50" placeholder="password"/>
<br/>
<input type="submit" value="Login" />
</form>
</body>
</html>