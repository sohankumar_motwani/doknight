<?php
require_once('../../includes/database_master.inc.php');
include '../../includes/error_master.inc.php';

$database_master = new DatabaseMaster();

$rsltUser = "";
//$id = $_GET['id'];
if(!empty($_GET['id'])) 
{
$qryUser = "SELECT UserID, FirstName, LastName, Email, UserTypeID FROM users WHERE UserID = " . $_GET['id'];
$rsltUser = $database_master->querySelect($qryUser);
}

$qryRoles = "SELECT UserTypeID, Type FROM usertypes";
$rsltRoles = $database_master->querySelect($qryRoles);
?>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title><?php echo empty($_GET['id'])?"Create an Account":"Update Account - " . $_GET['id']; ?> | THC</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" type="text/css" media="screen" href="../../assets/styles/main.css" />
    <link rel="stylesheet" type="text/css" media="screen" href="../../assets/styles/bootstrap.min.css" />
<script src="../../assets/scripts/main.js"></script>
</head>
<body>
    <h1><?php echo empty($_GET['id'])?"Create an Account":"Update Account - " . $_GET['id']; ?></h1>
<ul><li><?php echo $message; ?></li></ul>
<form action="process/create-submit.php" method="post">
<?php if(!empty($_GET['id'])): ?>
<input type="hidden" <?php if(count($rsltUser)) echo " value='".$rsltUser[0]['UserID'] . "'"; ?> name="idPosted"/>
<?php endif; ?>
<label>First Name <span>*</span>: </label>
<input type="text" name="firstName" required="required" id="firstName" maxlength="50" placeholder="John" <?php if(!empty($_GET['id'])) echo " value='".$rsltUser[0]['FirstName']."'";?>/>
<br/>
<label>Last Name <span>*</span>: </label>
<input type="text" name="lastName" required="required" id="lastName" maxlength="50" placeholder="Doe" <?php if(!empty($_GET['id'])) echo " value='".$rsltUser[0]['LastName']."'"; ?>/>
<br/>
<label>E-mail <span>*</span>: </label>
<input type="email" name="email" required="required" id="email" maxlength="50" placeholder="someone@example.com" <?php if(!empty($_GET['id'])) echo " value='".$rsltUser[0]['Email']."'"; ?>/>
<br/>
<label>Role <span>*</span>: </label>
<select required="required" name="role">
<option value="">Select...</option>
<?php if(count($rsltRoles) && is_array($rsltRoles)) {
foreach($rsltRoles as $key => $result) { ?>
<option value="<?php echo $result['ID']; ?>" <?php if(!empty($_GET['id'])) if($rsltUser[0]['UserTypeID'] == $result['ID']) echo ' selected="selected"'; ?>><?php echo $result["Type"]; ?></option>
<?php }
} ?>
</select>
<br/>
<label>Profile Picture: </label>
<input type="file" name="filePosted" />
<input type="submit" value="Submit" />
</form>
</body>
</html>