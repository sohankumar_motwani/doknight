<?php
$message = "";

if(!empty($_GET['error']))
{
switch($_GET['error'])
{
    case "already_exists":
    $message = "<label class='text-danger'>Error: there is already an account associated with the e-mail address you provided.</label>";
    break;
case "incomplete":
$message = "<label class='text-danger'>Error: you've provided an invalid value for one of the required fields. Please check the values provided and try again.</label>";
break;
case "incorrect_username":
$message = "<label class='text-danger'>Error: the username youor e-mail  provided doesn't exist.</label>";
break;
case "incorrect_password":
$message = "<label class='text-danger'>Error: the password provided doesn't match the username or e-mail specified.</label>";
break;
case "incorrect":
$message = "<label class='text-danger'>Error: the e-mail address and/or password provided is/are incorrect.</label>";
break;
case "unknown_record":
$message = "<label class='text-danger'>Error: you have selected or provided an invalid ID.</label>";
break;
default:
$message = "<label class='text-danger'>Error: an unknown error occured.</label>";
}
}
else
{
    $message = "<label class='text-info'>Note: the default password for each user you will be creating would be <b><u>@Thc123</u></b>. Account holders may then opt to change their respective passwords as necessary.</label>";
}
?>