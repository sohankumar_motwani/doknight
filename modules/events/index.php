<?php
require_once('../../includes/database_master.inc.php');
include '../../includes/error_master.inc.php';

$database_master = new DatabaseMaster();

$qryUsers = "SELECT u.UserID, CONCAT(u.FirstName, ' ', u.LastName) as FullName, u.Email, u.IsActivated, r.Type FROM users as u INNER JOIN usertypes as r ON r.UserTypeID = u.UserTypeID";
$rsltUsers = $database_master->querySelect($qryUsers);
?>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Users | DOKNIGHT</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" type="text/css" media="screen" href="../../assets/styles/main.css" />
    <link rel="stylesheet" type="text/css" media="screen" href="../../assets/styles/bootstrap.min.css" />
<script src="../../assets/scripts/main.js"></script>
</head>
<body>
<h1>Users</h1><a href="create.php">Register</a>
<ul><li><?php echo $message; ?></li></ul>
<table>
<thead>
<th>Name</th>
<th>E-mail</th>
<th>Role</th>
<th>Status</th>
<th></th>
</thead>
<tbody>
<?php if(is_array($rsltUsers) && count($rsltUsers)):
foreach($rsltUsers as $key => $result): ?>
<tr>
<td><?php echo $result['FullName']; ?></td>
<td><?php echo $result['Email']; ?></td>
<td><?php echo $result['Type']; ?></td>
<td><?php echo $result['Status']; ?></td>
<td><a href="create.php?id=<?php echo $result['UserID']; ?>">Update</a> | <a href="delete.php?id=<?php echo $result['UserID']; ?>" onclick="return confirm('Are you sure you wish to delete this user?')">Delete</a></td>
</tr>
<?php endforeach; 
else: ?>
    <tr><td colspan="6"><h1>No users registered yet.</h1></td></tr>
<?php endif; ?>
</tbody>
</table>
</body>
</html>
<script type="text/javascript">
var onDeleteUser = (id) => 
{
    var action = confirm("Are you sure you wish to delete this user?");
if(action)
window.location.href = "delete.php?id="+id;
}
</script>