DROP DATABASE doknight;
CREATE DATABASE doknight;
USE doknight;

CREATE TABLE usertypes (
UserTypeID int AUTO_INCREMENT PRIMARY KEY NOT NULL,
Type varchar(50) NOT NULL
);

CREATE TABLE users (
UserID int AUTO_INCREMENT PRIMARY KEY NOT NULL,
FirstName varchar(50) NOT NULL,
LastName varchar(50) NOT NULL,
Address varchar(170) NOT NULL,
ContactNumber char(11) NOT NULL,
Email varchar(120) NOT NULL,
Password varchar(64) NOT NULL,
BloodType char(5) NOT NULL,
ProfilePicture varchar(80),
DateRegistered DateTime NOT NULL,
DateModified DateTime,
UserTypeID int NOT NULL,
IsActivated tinyint(1) NOT NULL
);

CREATE TABLE events (
EventID int AUTO_INCREMENT PRIMARY KEY NOT NULL,
Name varchar(85) NOT NULL,
Description varchar(250) NOT NULL,
DateStart Date NOT NULL,
DateEnd Date NOT NULL,
TimeStart Time NOT NULL,
TimeEnd Time NOT NULL,
VenueAddress varchar(170) NOT NULL,
Collats varchar(120) NOT NULL
);

CREATE TABLE user_events (
UserEventID int AUTO_INCREMENT PRIMARY KEY NOT NULL,
UserID int NOT NULL,
EventID int NOT NULL,
DateDonated DateTime NOT NULL
);


ALTER TABLE users ADD CONSTRAINT FK_UserType FOREIGN KEY (UserTypeID) REFERENCES usertypes(UserTypeID);
ALTER TABLE user_events ADD CONSTRAINT FK_UserEvent FOREIGN KEY (UserID) REFERENCES users(UserID);
ALTER TABLE user_events ADD CONSTRAINT FK_EventUser FOREIGN KEY (EventID) REFERENCES events(EventID);
